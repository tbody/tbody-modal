# tbody-modal
Плагин для создания модальных окон. При открытии модального окна фокус
 переводится на первый элемент формы (если отмечен флаг `catchFocus: true`).
 Плагин представляет собой ES6 class.

## Установка
`npm install tbody-modal --save`

##Подключение
```js
import Tbodymodal from 'tbody-modal'
```

##Вызов
```js
const myModal = new Tbodymodal(selector, settings);
```
где `selector` - это селектор форм которые необходимо валидировать, `settings` - объект настроек


##Пример использования

**HTML**
```html
<button id="show-modal">Показать модальное окна</button>
<div class="modal">
    <div class="modal__backdrop"></div>
    <div class="modal__content">
        <button class="modal__close" >Закрыть</button>
        <h3>Hello world</h3>
    </div>
</div>
```
**JavaScript**
```js
const button = document.getElementById('show-modal');
const myModal = new Tbodymodal('.modal');

button.addEventListener('click', (e) => {
    e.preventDefault(); 
    myModal.show();
});
```

## Настройки

### Селекторы модального окна
В плагине реализована возможность изменения имен классов для каждого
элемента модального окна и других сопутствующий элементов.

| Опция             | Значение по-умолчанию  | Описание                                                                                                     |
|-------------------|------------------------| -------------------------------------------------------------------------------------------------------------|
| modalContent      | `modal__content`       | Контейнер контентной части модального окна                                                                   |
| modalClose        | `modal__close`         | Кнопка закрытия                                                                                              |
| bodyWithOpenModal | `has-open-tbody-modal` | Класс добавляемый `body` для запрета прокрутки                                                               |


Пример
```javascript
const myModal = new Tbodymodal('.modal', {
    classNames: {
        modalBackdrop: 'modal__backdrop',
        modalContent: 'modal__content',
        modalClose: 'modal__close',
        bodyWithOpenModal: 'has-open-tbody-modal',
    }
});
```

### Способы закрытия модального окна
Есть возможность изменить способы закрытия окна

| Опция             | Значение по-умолчанию | Описание                                         |
|-------------------|-----------------------| -------------------------------------------------|
| backdrop          | `true`                | Клик по заднему фону модального ок               |
| button            | `true`                | Клик по кнопка заданной через класс `modalClose` |
| esc               | `true`                | Нажатие клавиши `esc`                            |


Пример
```javascript
const myModal = new Tbodymodal('.modal', {
    closeVariant: {
        backdrop: false,
        esc: false,
    }
});
```


### Анимация
В данном плагине реализована возможность анимирования появления и исчезновения модального окна.
Анимация осуществаяется с помощью CSS3 Анимации. Параметры анимации указаны в файле 
`style.css`. Добавлены четыре эффекта: `slideInTop`, `slideInLeft`, `slideOutRight`,
`slideOutDown`. 

| Опция         | Значение по-умолчанию | Описание                                               |
|-------------- |-----------------------| -------------------------------------------------------|
| modalIn       | `modal_in`            | Класс добавляемый при появлении модального окна    |
| modalOut      | `modal_out`           | Класс добавляемый при исчезновении модального окна |

Пример
```javascript
const myModal = new Tbodymodal('.modal', {
    classNames: {
        modalIn: 'modal_in',
        modalOut: 'modal_out',
    }
});
```

### События

| Событие            | Описание                                        |
| ------------------ | ------------------------------------------------|
| tbody-modal.show   | Вызывается перед появлением модального окна     |
| tbody-modal.shown  | Вызывается после появления окна.                |
| tbody-modal.close  | Вызывается перед исчезновении модального окна   |
| tbody-modal.closed | Вызывается после исчезновении окна.             |


Пример
```javascript
const myModal = new Tbodymodal('.modal');

myModal.DOM.modal.addEventListener('tbody-modal.show', () => {
    console.log('show ');
});
```

### Методы
| Метод          | Описание                     |
| -------------- | -----------------------------|
| show           | Показывает модальное окно    |
| close          | Скрывает модальное окно      |

Пример
```js
const myModal = new Tbodymodal('.modal');
myModal.close();
```


## Глобальные методы

### Методы
| Метод          | Описание                     |
| -------------- | -----------------------------|
| closeAll       | Скрывает все модальные окна  |

Пример
```js
import Tbodymodal from 'tbody-modal'
Tbodymodal.closeAll();
```
