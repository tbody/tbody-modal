import Tbodymodal from '../src/script';

const button = document.getElementById('open-modal');
const button2 = document.getElementById('open-modal-2');
const closeAll = document.getElementById('close-all');

const myModal = new Tbodymodal('#modal1', {
  closeVariant: {
    backdrop: false,
  },
});

const myModal2 = new Tbodymodal('#modal2');

button.addEventListener('click', (e) => {
  e.preventDefault();
  myModal.show();
});

button2.addEventListener('click', (e) => {
  e.preventDefault();
  myModal2.show();
});

closeAll.addEventListener('click', () => {
  Tbodymodal.closeAll();
});

myModal.DOM.modal.addEventListener('tbody-modal.show', () => {
  console.log('show');
});

myModal.DOM.modal.addEventListener('tbody-modal.shown', () => {
  console.log('shown');
});


myModal.DOM.modal.addEventListener('tbody-modal.close', () => {
  console.log('close');
});

myModal.DOM.modal.addEventListener('tbody-modal.closed', () => {
  console.log('closed ');
});

const input = document.querySelector('[name="name"]');

input.addEventListener('focus', (e) => {
  console.log(e.target);
});
