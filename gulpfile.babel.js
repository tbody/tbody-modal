import autoprefixer from 'gulp-autoprefixer';
import gulp from 'gulp';
import plumber from 'gulp-plumber';
import sass from 'gulp-sass';


gulp.task('scss', () => (
  gulp.src('./demo/style.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 10 version'],
    }))
    .pipe(gulp.dest('./demo'))
));

gulp.task('watch-scss', () => (
  gulp.watch([
    './demo/style.scss',
    './src/styles/*.scss',
    './src/styles/**/*.*',
  ], gulp.series('scss'))
));
