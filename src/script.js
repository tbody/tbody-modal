const deepAssign = require('deep-assign');

const instances = [];

/* Класс представляющий Tbodymodal */
class Tbodymodal {

  /**
   * Создание Tbodymodal
   * @param {string} selector - Селектор модального окна
   * @param {object} settings - Настройки модального окна
   * */
  constructor(selector, settings) {
    this.defaults = {

      classNames: {
        modalBackdrop: 'modal__backdrop',
        modalContent: 'modal__content',
        modalClose: 'modal__close',

        modalIn: 'modal_in',
        modalOut: 'modal_out',

        bodyWithOpenModal: 'has-open-tbody-modal',
      },

      closeVariant: {
        backdrop: true,
        button: true,
        esc: true,
      },

    };

    this.events = {};
    this.DOM = {};

    // Названия событий модального окна
    this.eventsNames = {
      show: 'tbody-modal.show',
      shown: 'tbody-modal.shown',
      close: 'tbody-modal.close',
      closed: 'tbody-modal.closed',
    };

    // Названия transition events
    this.variantsTransitionsNames = {
      transition: 'transitionend',
      OTransition: 'oTransitionEnd',
      MozTransition: 'transitionend',
      WebkitTransition: 'webkitTransitionEnd',
    };

    // Настройки
    this.settings = deepAssign(this.defaults, settings);

    this.init(selector);
  }

  /**
   * Закрывает модальное окно
   * */
  close() {
    const classNames = this.settings.classNames;

    this.DOM.modal.dispatchEvent(this.events.close);

    this.DOM.modal.classList.remove(classNames.modalIn);
    this.DOM.modal.classList.add(classNames.modalOut);

    this.settings.isVisible = false;
  }

  /**
   * Закрывает модальное окно которое находится ниже по DOM дереву
   * Нужно для закрытия окна при нажатии Esc когда открыто несколько окон
   * */
  closeForwardModal() {
    for (let i = instances.length - 1; i >= 0; i -= 1) {
      const isOpen = instances[i].DOM.modal.classList.contains(this.settings.classNames.modalIn);
      if (isOpen) {
        this.close();
        break;
      }
    }
  }

  /**
   * Компенсирует ширину скролла, чтобы устранить "перескоки" ширины содержимого
   * */
  compensationScrollbarWidth() {
    const isHasScrollbar = document.body.scrollHeight > window.innerHeight;
    const scrollbarSize = this.getScrollbarSize();

    if (isHasScrollbar && scrollbarSize) {
      document.documentElement.style.marginRight = `${scrollbarSize}px`;
    }
  }

  /**
   * Делегирует события в зависимости он наличи CSS-класса
   * */
  delegateTriggerEvents() {
    const isModalShown = this.DOM.modal.classList.contains(this.settings.classNames.modalIn);

    if (isModalShown) {
      this.handlerShown();
    } else {
      this.handlerClosed();
    }
  }

  /**
   * Добавляет обработчики событий на элементы модального окна и клавиатуру
   * */
  eventListener() {
    const closeVariant = this.settings.closeVariant;

    if (closeVariant.backdrop) {
      // Прослушиватель на backdrop
      this.DOM.modalBackdrop.addEventListener('click', () => {
        this.close();
      });
    }

    if (closeVariant.button) {
      // Прослушиватель на кнопке 'Закрыть'
      this.DOM.modalClose.addEventListener('click', () => {
        this.close();
      });
    }

    if (closeVariant.esc) {
      // Прослушиватель на кнопке Escape
      window.addEventListener('keydown', (e) => {
        if (e.keyCode === 27) {
          this.closeForwardModal();
        }
      });
    }

    // Прослушиватель на оконачание события transitionend на CSS свойство visibility
    this.DOM.modal.addEventListener(this.transitionName, (e) => {
      if (e.propertyName === 'visibility') {
        this.delegateTriggerEvents();
      }
    });
  }

  /**
   * Находит все элементы модального окна
   * @param {string} selector - Селектор модального окна
   * */
  findElements(selector) {
    const classNames = this.settings.classNames;
    this.DOM.modal = document.querySelector(selector);
    this.DOM.modalBackdrop = this.DOM.modal.getElementsByClassName(classNames.modalBackdrop)[0];
    this.DOM.modalContent = this.DOM.modal.getElementsByClassName(classNames.modalContent)[0];
    this.DOM.modalClose = this.DOM.modal.getElementsByClassName(classNames.modalClose)[0];
  }

  /**
   * Определяет ширину скролбара
   * @return {number} - Возвращает ширину скролбара
   * */
  getScrollbarSize() {
    if (this.settings.scrollbarSize === undefined) {
      const scrollDiv = document.createElement('div');
      scrollDiv.style.cssText = `width: 99px;
                                    height: 99px;
                                    overflow:scroll;
                                    position:absolute;
                                    top: -9999px;`;

      document.body.appendChild(scrollDiv);
      this.settings.scrollbarSize = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
    return this.settings.scrollbarSize;
  }

  /**
   * Обработчик события закрытия окна
   * */
  handlerClosed() {
    const classNames = this.settings.classNames;
    this.DOM.modal.dispatchEvent(this.events.closed);
    this.DOM.modal.classList.remove(classNames.modalOut);
    if (instances.every(el => !el.settings.isVisible)) {
      document.documentElement.classList.remove(classNames.bodyWithOpenModal);
      document.documentElement.style.marginRight = '';
    }
  }

  /**
   * Обработчик события открытия окна
   * */
  handlerShown() {
    this.DOM.modal.dispatchEvent(this.events.shown);
  }

  /**
   * Инициализация
   * @param {string} selector - Селектор модального окна
   * */
  init(selector) {
    this.findElements(selector);
    this.registerEvents();
    this.wichTransitionEvent();
    this.eventListener();
    instances.push(this);
  }

  /**
   * Регистрирует кастомные события
   * */
  registerEvents() {
    Object.keys(this.eventsNames).forEach((item) => {
      const newEvent = document.createEvent('Event');
      newEvent.initEvent(this.eventsNames[item], true, false);
      this.events[item] = newEvent;
    });
  }

  /**
   * Показывает модальное окно
   * */
  show() {
    const classNames = this.settings.classNames;
    this.compensationScrollbarWidth();

    this.DOM.modal.dispatchEvent(this.events.show);
    this.DOM.modal.classList.remove(classNames.modalOut);
    this.DOM.modal.classList.add(classNames.modalIn);
    this.settings.isVisible = true;

    document.documentElement.classList.add(classNames.bodyWithOpenModal);
  }

  /**
   * Определяет имя (возможен префикс) события transitionend
   * */
  wichTransitionEvent() {
    const el = document.createElement('fakeelement');
    Object.keys(this.variantsTransitionsNames).forEach((item) => {
      if (el.style[item] !== undefined) {
        this.transitionName = this.variantsTransitionsNames[item];
      }
    });
  }
}

// Метод для закрытия всех модальных окон
Tbodymodal.closeAll = () => instances.map(item => item.close());

export default Tbodymodal;

