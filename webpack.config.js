const webpack = require('webpack');
const path = require('path');


const config = {
  entry: {
    script: './demo/script',
  },
  output: {
    path: path.resolve(__dirname, 'demo'),
    filename: '[name].min.js',
  },
  devtool: 'cheap-module-eval-source-map',
  module: {
    rules: [
      {
        test: /\.js/,
        exclude: [
          path.resolve(__dirname, 'node_modules'),
        ],
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: true,
    }),
  ],
};

module.exports = config;
